/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Daniel Barra
 */
public class TiposAlarma {
    private int IdAlarma;
    private String NombreAlarma;
    private String Descripcion;

    @Override
    public String toString() {
        return "TiposAlarma{" + "IdAlarma=" + IdAlarma + '}';
    }

    public int getIdAlarma() {
        return IdAlarma;
    }

    public void setIdAlarma(int IdAlarma) {
        this.IdAlarma = IdAlarma;
    }

    public String getNombreAlarma() {
        return NombreAlarma;
    }

    public void setNombreAlarma(String NombreAlarma) {
        this.NombreAlarma = NombreAlarma;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public TiposAlarma(int IdAlarma, String NombreAlarma, String Descripcion) {
        this.IdAlarma = IdAlarma;
        this.NombreAlarma = NombreAlarma;
        this.Descripcion = Descripcion;
    }
    
}
